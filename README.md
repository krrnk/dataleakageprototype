# DataLeakagePrototype

Repository for the Data Leakage study by the Future Methods research project at Aalto University

## Installation

### Clone this repository
```
git clone https://gitlab.com/krrnk/dataleakageprototype.git
cd dataleakageprototype/ 
git checkout dev
```

### Install dependencies
#### Client App
- Download and install [Node.js](https://nodejs.dev/en/download/)

- Change directory
```
cd /path/to/repository/digital-prototype/
```
- Install node modules using npm or yarn
```
npm install
```
or 
```
yarn
yarn install
```

#### Server app
- Download and install [Python 3.11](https://www.python.org/downloads/)
- Install Pipenv
```
python3 -m pip install pipenv
```
- Change directory to the repository's root directory
```
cd /path/to/repository/
```
- Create a Python environment with Python 3.11
```
pipenv --python 3.11
pipenv shell
```
- Install python modules
```
pipenv install
```

## Usage
- Open a terminal tab for the server
```
cd /path/to/repository/
pipenv shell
cd digital-prototype/backend/
python server.py
```
- Open another terminal tab for the client app and change directory to `digital-prototype/`
```
cd /path/to/repository/digital-prototype
npm start
```

- OPTIONAL: Working on the physical prototype and pyboard
	- Connect the pyboard to your computer
	- Connect to the pyboard via rshell
	```
	rshell -p /dev/tty[pyboard port] # for example /dev/ttyACM0
	```
	- Copy files to the pyboard
	```
	cp /path/to/file /pyboard/
	```	
	- Copy files from the pyboard
	```
	cp /pyboard/path/to/file /path/to/destination
	```	
	- Launching micropython
	```
	repl
	```
	- [rshell documentation](https://github.com/dhylands/rshell)
	

## Project structure
```
| (launch `pipenv shell` always from the repository's root) 
|
|-- digital-prototype (launch `npm start` always from here)
|  |-- src: editable js && css files
|  |-- backend: editable python files and json files
|  |-- public: editablle html file
|  |-- package.json
|
|-- physical-prototype: editable python files
|  |-- pico_libs: micropython libraries
| 
|-- Pipfile
|-- README.md
|-- LICENSE
```


## Roadmap
[  ] Implement timeseries for labels and diary entries

[  ] Implement diary entries

[  ] Implement system update

[  ] Overall styling

## Contributing
- Use the `main` branch only for major changes. Versions should work but can have some minor issues.
- Use the `dev` branch for development versions that work but may have issues.
- Create your own branch to work on feature development.
- Always merge to `dev` before mergin to `main`.

### Variable naming conventions
- Python: snake_case
- JS: camelCase
- Explicit variable and function names are preferred over comments. 

### Reactjs 
Use hooks isntead of classes. For more information see [React hook guide](https://reactjs.org/docs/hooks-intro.html) and [React docs beta](https://beta.reactjs.org/learn#using-hooks)

## Authors and acknowledgment
This work is supported by the Academy of Finland project grant 330124

## License
[GNU GPLv3](./LICENSE)

## Project status
Work in progress
